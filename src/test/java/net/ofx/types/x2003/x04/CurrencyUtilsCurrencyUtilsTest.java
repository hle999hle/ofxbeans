package net.ofx.types.x2003.x04;

import org.junit.Test;

import junit.framework.Assert;

public class CurrencyUtilsCurrencyUtilsTest {
    @Test
    public void testCurrencyEnum() {
        Assert.assertEquals("USD", CurrencyEnum.USD.toString());
    }
}
